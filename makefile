LD = ld
LD_FLAGS = -o
ASM = nasm
ASM_FLAGS = -f elf64 -o
PY = python3
main: main.o lib.o dict.o
	$(LD) -o $@ $^
%.o: %.asm
	$(ASM) $(ASM_FLAGS) $@ $<
clean:
	rm -rf *.o
	rm main
test: main
	$(PY) test.py
.PHONY: clean test
