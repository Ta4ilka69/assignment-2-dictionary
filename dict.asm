%macro pushregs 1-*
    %rep %0
        push %1
        %rotate 1
    %endrep
%endmacro

%macro popregs 1-*
    %rep %0
        %rotate -1
        pop %1
    %endrep
%endmacro

section .text
global find_word
%include "lib.inc"
;rdi - string
;rsi - dict
;byte[next] byte[null-terminated-key] 2byte[null-terminated-string]
find_word:
    ;convention save
    pushregs r12,r13
    mov r12, rdi
    mov r13, rsi
    .loop:
        test rsi, rsi
        jz .not_found
        add rsi, 8 ; skip to key
        call string_equals
        test rax, rax ; rax = 0 if not equals
        jz .next_key
        mov rax,r13 ; return ref if found
        jmp .exit
    .next_key:
        mov rsi, [r13] ; mov to next
        mov r13, rsi
        jmp .loop
    .not_found:
        xor rax,rax ; 0 if not found
    .exit:
        popregs r12,r13 ;convention restore
        ret