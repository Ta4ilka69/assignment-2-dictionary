%include "dict.inc"
%include "lib.inc"
%include "words.inc"
%define BUFFER_LEN 255
section .rodata
    error_msg db "Error: string is too long for reading!",10,0
    not_found_msg db "No such key found!",10,0
section .bss
    buffer resb BUFFER_LEN ;uninitialized buffer for reading
section .text
global _start
_start:
    mov rdi, buffer
    mov rsi, BUFFER_LEN
    call read_word
    test rax,rax
    jz .bad_input ;too long string
    .finding_word:
    mov rdi, buffer 
    mov rsi, dict
    call find_word
    test rax,rax
    jz .not_found
    add rax,8 ;get string address
    mov rdi, rax
    push rdi
    call string_length ;getting size of key
    pop rdi
    add rdi, rax ;getting address of value
    inc rdi ;skip null byte
    call print_string
    jmp .end
    .not_found:
        mov rdi, not_found_msg
        call print_string_err
        jmp .end
    .bad_input:
        mov rdi, error_msg
        call print_string_err ;printing error message to stderr
    .end:
        jmp exit ; jmp instead of call
